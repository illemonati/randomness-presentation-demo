import React, {useState} from "react";
import {Container, FormControl, InputLabel, MenuItem, Paper, Select, Tab, Tabs, Typography} from '@material-ui/core';
import SwipeableViews from "react-swipeable-views";
import RandomNumberGeneratorResultsComponent from "./RandomNumberGeneratorResultsComponent";
import generatorTypes from './generator-types.json';

import RandomNumberGeneratorAnalysisComponent from "./RandomNumberGeneratorAnalysisComponent";
import LCGInputsModule from "./input-modules/LCGInputsModule";
import MathRandomInputModule from "./input-modules/MathRandomInputModule";
import Rule30InputModule from "./input-modules/Rule30InputsModule";
import RandomOrgInputModule from "./input-modules/RandomOrgInputModule";
import QuantumInputModule from "./input-modules/QuantumInputModules";

const RandomNumbersGeneratorComponent = () => {


    const [tabVal, setTabVal] = useState(0);
    const [numbers, setNumbers] = useState([1, 2, 3, 4] as number[]);
    const [generatorSelected, setGeneratorSelected] = useState(0);

    const handleTabChange = (e: any, newValue: number) => {
        setTabVal(newValue);
    };

    const getInputModule = () => {
        switch (generatorTypes[generatorSelected]) {
            case 'Quantum (simulated)':
                return (<QuantumInputModule callback={setNumbers}/>);
            case 'Atmospheric Noise (Random.org)':
                return (<RandomOrgInputModule callback={setNumbers}/>);
            case 'Rule30':
                return (<Rule30InputModule callback={setNumbers}/>);
            case 'Javascript Math.random()':
                return (<MathRandomInputModule callback={setNumbers}/>);
            case 'LCG':
                return (<LCGInputsModule callback={setNumbers}/>);
            default:
                return (<LCGInputsModule callback={setNumbers}/>);
        }
    };

    const inputs = (
        <div className="inputs">
            <Paper>
                <br />
                <br />


                <FormControl variant="outlined" className="generatorTypeFormControl">
                    <InputLabel>Generator Type</InputLabel>
                    <Select value={generatorSelected}
                            onChange={(e) => setGeneratorSelected(e.target.value as number)}
                            label="Generator Type"
                    >
                        {generatorTypes.map((generatorType: string, i) => {
                            return (
                                <MenuItem value={i} key={i}>{generatorType}</MenuItem>
                            )
                        })}
                    </Select>
                </FormControl>
                <br />
                <br />
                {getInputModule()}
            </Paper>
        </div>
    );

    return (
        <div className="LCGComponent">
            <Container>
                <Typography variant="h3">
                    Random Positive Integer Generator
                </Typography>
                <br />
                <br />
                <Paper>
                    <Tabs value={tabVal}
                          onChange={handleTabChange}
                          centered
                          textColor="primary"
                    >
                        <Tab label="inputs" />
                        <Tab label="results" />
                        <Tab label="analysis" />
                    </Tabs>
                </Paper>
                <br />
                <br />
                <SwipeableViews index={tabVal} onChangeIndex={(i) => setTabVal(i)}>
                    {inputs}
                    <RandomNumberGeneratorResultsComponent numbers={numbers}/>
                    <RandomNumberGeneratorAnalysisComponent numbers={numbers} />
                </SwipeableViews>
            </Container>
        </div>
    )

};

export default RandomNumbersGeneratorComponent;

