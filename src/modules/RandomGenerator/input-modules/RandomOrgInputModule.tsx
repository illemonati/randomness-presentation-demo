import React, {ChangeEvent, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";
import * as RandomOrg from 'random-org';
import randomOrgConfig from '../../../configs/randomorgConfig.json';

interface RandomOrgInputModuleProps  {
    callback: (numbers: number[]) => any,
}

const RandomOrgInputModule = (props: RandomOrgInputModuleProps) => {

    const twoNumbers = [Math.floor(Math.random()*100000), Math.floor(Math.random()*100000)];

    const [variables, setVariables] = useState({
        'max': Math.max(...twoNumbers),
        'min': Math.min(...twoNumbers),
        'amount': 50
    } as any);

    const handleVariableChange = (variable: string) => (event: ChangeEvent<HTMLInputElement>) => {
        const vars = {...variables};
        const val = parseInt(event.target.value);

        if (val < 0) {
            vars[variable] = 0;
        } else if (variable === 'amount' && val > 1000) {
            vars[variable] = 1000;
        } else if (val > 100000) {
            vars[variable] = 100000;
        } else {
            vars[variable] = val;
        }
        setVariables(vars);
    };

    useEffect(() => {
        const randomOrg = new RandomOrg(
            randomOrgConfig
        );

        randomOrg.generateIntegers({
            n: variables.amount,
            min: variables.min,
            max: variables.max,
        }).then((response: any) => {
            props.callback(response.random.data);
        })

        //eslint-disable-next-line
    }, [variables]);


    return (
        <>
            {Object.keys(variables).map((variable, i) => {
                return (
                    <React.Fragment key={i}>
                        <TextField type='number'
                                   value={variables[variable]}
                                   label={variable}
                                   className="inputField"
                                   onChange={handleVariableChange(variable)}
                                   variant='outlined'
                        />
                        <br />
                        <br />
                    </React.Fragment>
                )
            })}
        </>
    )
};

export default RandomOrgInputModule;


