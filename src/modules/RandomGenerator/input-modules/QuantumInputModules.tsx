import React, {ChangeEvent, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";
import * as QuantumCircuit from 'quantum-circuit';

interface QuantumInputModuleProps  {
    callback: (numbers: number[]) => any,
}

const QuantumInputModule = (props: QuantumInputModuleProps) => {

    const [variables, setVariables] = useState({
        'number of qubits': 8,
        'amount': 10
    } as any);

    const [svgSrc, setSVGSrc] = useState('');

    const handleVariableChange = (variable: string) => (event: ChangeEvent<HTMLInputElement>) => {
        const vars = {...variables};
        const val = parseInt(event.target.value);
        if (val < 1 || isNaN(val)) {
            vars[variable] = 1;
        } else if (variable === 'number of qubits' && val > 10) {
            vars[variable] = 10;
        } else if (val > 1000) {
            vars[variable] = 1000;
        } else {
            vars[variable] = val;
        }
        setVariables(vars);
    };

    useEffect(() => {
        let nums = [] as number[];
        const circuit = new QuantumCircuit();
        for (let j = 0; j < variables['number of qubits']; ++j) {
            circuit.addGate('h', -1, j);
            circuit.addMeasure(j, 'c', j);
        }

        const blob = new Blob([circuit.exportSVG(true)], {type: 'image/svg+xml'});
        setSVGSrc(URL.createObjectURL(blob));

        for (let i = 0; i < variables.amount; ++i) {
            circuit.run();
            nums.push(circuit.getCregValue('c'));
        }

        props.callback(nums);
        //eslint-disable-next-line
    }, [variables]);


    return (
        <>
            {Object.keys(variables).map((variable, i) => {
                return (
                    <React.Fragment key={i}>
                        <TextField type='number'
                                   value={variables[variable]}
                                   label={variable}
                                   className="inputField"
                                   onChange={handleVariableChange(variable)}
                                   variant='outlined'
                        />
                        <br />
                        <br />
                    </React.Fragment>
                )
            })}
            <img className="svgImg" alt="circuit diagram" src={svgSrc} />
            <br />
            <br />
        </>
    )
};

export default QuantumInputModule;


