import React, {useEffect, useState} from "react";
import {Container, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {piEstimationTest} from "./utils";


interface LCGAnalysisComponentProps {
    numbers: number[]
}


const RandomNumberGeneratorAnalysisComponent = (props: LCGAnalysisComponentProps) => {


    const [piEstimation, setPiEstimation] = useState('calculating');

    useEffect(() => {
        piEstimationTest(props.numbers).then(res => setPiEstimation(res.toString()));
    }, [props]);


    return (
        <div className="LCGAnalysisComponent">
            <Container>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Test Conducted</TableCell>
                                <TableCell>Result</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell>
                                    π estimation test
                                </TableCell>
                                <TableCell>
                                    π ≈ {piEstimation}
                                </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
            </Container>
        </div>
    )
};


export default RandomNumberGeneratorAnalysisComponent;
