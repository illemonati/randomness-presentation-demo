import React, {ChangeEvent, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";

interface MathRandomInputModuleProps  {
    callback: (numbers: number[]) => any,
}

const MathRandomInputModule = (props: MathRandomInputModuleProps) => {

    const twoNumbers = [Math.floor(Math.random()*100000), Math.floor(Math.random()*100000)];

    const [variables, setVariables] = useState({
        'max': Math.max(...twoNumbers),
        'min': Math.min(...twoNumbers),
        'amount': 50
    } as any);

    const handleVariableChange = (variable: string) => (event: ChangeEvent<HTMLInputElement>) => {
        const vars = {...variables};
        const val = parseInt(event.target.value);
        if (val < 0) {
            vars[variable] = 0;
        } else if (val > 100000) {
            vars[variable] = 100000;
        } else {
            vars[variable] = val;
        }
        setVariables(vars);
    };

    useEffect(() => {
        let nums = [] as number[];
        for (let i = 0; i < variables.amount; ++i) {
            const number = Math.random() * (variables.max - variables.min) + variables.min;
            nums.push(Math.floor(number));
        }
        props.callback(nums)
        //eslint-disable-next-line
    }, [variables]);


    return (
        <>
            {Object.keys(variables).map((variable, i) => {
                return (
                    <React.Fragment key={i}>
                        <TextField type='number'
                                   value={variables[variable]}
                                   label={variable}
                                   className="inputField"
                                   onChange={handleVariableChange(variable)}
                                   variant='outlined'
                        />
                        <br />
                        <br />
                    </React.Fragment>
                )
            })}
        </>
    )
};

export default MathRandomInputModule;


