import React from 'react';
import './App.css';
import RandomNumbersGeneratorComponent from "./modules/RandomGenerator/RandomNumbersGeneratorComponent";

function App() {
  return (
    <div className="App">
        <br />
        <br />
        <RandomNumbersGeneratorComponent/>
    </div>
  );
}

export default App;
