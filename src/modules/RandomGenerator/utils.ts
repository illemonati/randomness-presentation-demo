
export function * LCG(a: number, c: number, m: number, x0: number) {
    while (true) {
        x0 = (a * x0 + c) % m;
        yield x0;
    }
}

export async function piEstimationTest(numbers: number[]) {
    const normalizedNumbers = await normalizeNumbersBetween0and1(numbers);
    const n = normalizedNumbers.length - 1;
    let m = 0;
    for (let i = 0; i < normalizedNumbers.length-2; ++i) {
        const a = normalizedNumbers[i];
        const b = normalizedNumbers[i + 1];
        if ((a**2 + b**2) < 1) {
            m += 1;
        }
    }
    return (4 * m) / n;

}

export async function normalizeNumbersBetween0and1(numbers: number[]) {
    const max = Math.max(...numbers);
    const min = Math.min(...numbers);
    const maxmin = max - min;
    return numbers.map((number) => {
       return (number - min) / maxmin;
    });
}