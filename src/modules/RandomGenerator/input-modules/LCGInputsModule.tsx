import React, {ChangeEvent, useEffect, useState} from "react";
import {TextField} from "@material-ui/core";
import {LCG} from "../utils";
import MathJax from 'react-mathjax2';
import mathJaxOptions from '../../../configs/mathJaxConfig.json';

interface LCGInputsModuleProps  {
    callback: (numbers: number[]) => any,
}

const LCGInputsModule = (props: LCGInputsModuleProps) => {
    const [variables, setVariables] = useState({
        'a': Math.floor(Math.random()*100000),
        'c': Math.floor(Math.random()*100000),
        'm': Math.floor(Math.random()*100000),
        'seed': Math.floor(Math.random()*100000),
        'amount': 50
    } as any);

    const handleVariableChange = (variable: string) => (event: ChangeEvent<HTMLInputElement>) => {
        const vars = {...variables};
        const val = parseInt(event.target.value);
        if (val < 0) {
            vars[variable] = 0;
        } else if (val > 100000) {
            vars[variable] = 100000;
        } else {
            vars[variable] = val;
        }
        setVariables(vars);
    };
    
    useEffect(() => {
        const lcg = LCG(variables.a, variables.c, variables.m, variables.seed);

        let nums = [] as number[];
        for (let i = 0; i < variables.amount; ++i) {
            nums.push(lcg.next().value as number);
        }
        props.callback(nums)
        //eslint-disable-next-line
    }, [variables]);


    return (
        <>
            {Object.keys(variables).map((variable, i) => {
                return (
                    <React.Fragment key={i}>
                        <TextField type='number'
                                   value={variables[variable]}
                                   label={variable}
                                   className="inputField"
                                   onChange={handleVariableChange(variable)}
                                   variant='outlined'
                        />
                        <br />
                        <br />
                    </React.Fragment>
                )
            })}
            <MathJax.Context input="ascii" options={mathJaxOptions}>
                <div>
                    <MathJax.Text text={ `$$ X_{n+1} = (${variables.a} X_n + ${variables.c}) $$` } />
                    <MathJax.Text text={ `$$ X_0 = ${variables.seed} $$` } />
                </div>
            </MathJax.Context>
            <br />
            <br />
        </>
    )
};

export default LCGInputsModule;


