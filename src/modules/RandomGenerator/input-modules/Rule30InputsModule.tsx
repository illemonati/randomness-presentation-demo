import React, {ChangeEvent, useEffect, useState} from "react";
import {Button, TextField} from "@material-ui/core";

interface MathRandomInputModuleProps  {
    callback: (numbers: number[]) => any,
}

const Rule30InputModule = (props: MathRandomInputModuleProps) => {

    const [rule30ImgSrc, setRule30ImgSrc] = useState('');

    const [variables, setVariables] = useState({
        'width': 300,
        'height': 300,
    } as any);

    const [rowToStart, setRowToStart] = useState(Math.floor(Math.random() * variables.height));
    const [colToUse, setColToUse] = useState(Math.floor(Math.random() * variables.width));
    const [numbers, setNumbers] = useState([] as number[]);


    const handleVariableChange = (variable: string) => (event: ChangeEvent<HTMLInputElement>) => {
        const vars = {...variables};
        const val = parseInt(event.target.value);

        if (val < 0) {
            vars[variable] = 0;
        } else if (val > 1000) {
            vars[variable] = 1000;
        } else {
            vars[variable] = val;
        }
        setVariables(vars);
    };

    const generateNumber = () => {
        setRowToStart(Math.floor(Math.random() * variables.height));
        setColToUse(Math.floor(Math.random() * variables.width));


        runGame().then((bits) => {
            const bitString = bits.join('').replace('2', '1');
            const numberGenerated = parseInt(bitString, 2);
            const nums = numbers.slice();
            nums.push(numberGenerated);
            setNumbers(nums);
            props.callback(numbers);
        });
    };


    useEffect(() => {
        generateNumber();
        //eslint-disable-next-line
    }, [variables]);


    const runGame = async () : Promise<Uint8Array> => {

        const canvasElement = document.createElement('canvas');
        canvasElement.width = variables.width;
        canvasElement.height = variables.height;
        const ctx = canvasElement.getContext('2d')!;

        //@ts-ignore
        ctx.mozImageSmoothingEnabled = false;
        //@ts-ignore
        ctx.webkitImageSmoothingEnabled = false;
        //@ts-ignore
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;

        const colors = ["aqua", "fuchsia", "red"];
        let line = new Uint8Array(variables.width);
        const centerIndex = Math.floor(variables.width/2);
        line[centerIndex] = 1;


        const bits = new Uint8Array(variables.height);
        const next = (line: Uint8Array) => {
            const nextline = new Uint8Array(line.length);
            for ( let i = 0; i < variables.width; ++i) {
                const top = line[i];
                const right = line[i+1];
                const left = line[i-1];
                nextline[i] = (!top && !right) ? left : 1-left;
            }
            return nextline;
        };

        const drawLine = (line: Uint8Array, y: number) => {
            for (let x = 0; x < variables.width; ++x) {
                ctx.fillStyle = colors[line[x]];
                if ((x === colToUse) && (y > rowToStart) && (line[x] === 1)) {
                    ctx.fillStyle = colors[2];
                }
                ctx.fillRect(x, y, 1, 1,);
            }
        };

        for (let i = 0; i < variables.height; ++i) {
            line = next(line);
            bits[i] = line[colToUse];
            drawLine(line, i);
        }

        setRule30ImgSrc(canvasElement.toDataURL('img/jpeg', 0.1));

        return bits.slice(rowToStart);
    };


    return (
        <>
            {Object.keys(variables).map((variable, i) => {
                return (
                    <React.Fragment key={i}>
                        <TextField type='number'
                                   value={variables[variable]}
                                   label={variable}
                                   className="inputField"
                                   onChange={handleVariableChange(variable)}
                                   variant='outlined'
                        />
                        <br />
                        <br />
                    </React.Fragment>
                )
            })}

            <br />
            <br />
            <Button variant="outlined" onClick={() => generateNumber()}>
                Generate a number
            </Button>
            <br />
            <br />
            <br />
            <img src={rule30ImgSrc} alt="rule30" className="rule30Img" />
            <br />
            <br />
        </>
    )
};

export default Rule30InputModule;


