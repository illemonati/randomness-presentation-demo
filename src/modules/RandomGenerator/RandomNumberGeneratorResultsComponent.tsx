import React from "react";
import {FixedSizeList, ListChildComponentProps} from "react-window";
import {Container, ListItem, ListItemText, Paper} from "@material-ui/core";
import './styles.css';

interface LCGResultsComponentProps {
    numbers: number[]
}


const RandomNumberGeneratorResultsComponent = (props: LCGResultsComponentProps) => {
    const numbers = props.numbers;

    const renderRow = (props: ListChildComponentProps) => {
        const {index, style} = props;
        return (
            <ListItem button style={style} key={index} className='listItem'>
                <ListItemText primary={`${numbers[index]}`} className='listItemText' />
            </ListItem>
        )
    };

    return (
        <div className="LCGResultsComponent" style={{textAlign: 'center'}}>
            <Container className="listContainer">
                <Paper variant="outlined">
                    <FixedSizeList itemSize={46} height={400} itemCount={numbers.length} width={300} className="list">
                        {renderRow}
                    </FixedSizeList>
                </Paper>
            </Container>
            <br />
            <br />
        </div>
    );
};

export default RandomNumberGeneratorResultsComponent;

